package net.sbyrne.fig

import java.math.BigDecimal

sealed class FigValue {}

data class FigList( val list:MutableList<FigValue?> = mutableListOf() ): FigValue()

data class FigMap( val name:String? = null, val map:MutableMap<String?,FigValue?> = mutableMapOf() ): FigValue()

data class FigBoolean(val value:Boolean): FigValue() {
	companion object {
		val TRUE = FigBoolean(true)
		val FALSE = FigBoolean(false)
	}
}

data class FigString( val value:String ): FigValue()

data class FigNumber(val value:BigDecimal): FigValue() {
	constructor(value:String) : this(BigDecimal(value))
}
