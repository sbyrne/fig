package net.sbyrne.fig

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.*

fun FigValue?.toJsonNode(): JsonNode =
		when(this) {
			null -> NullNode.instance
			is FigMap -> ObjectNode(
					JsonNodeFactory.instance,
					map
							.asSequence()
							.map { it.key to it.value.toJsonNode() }
							.let { m ->
								if ( name != null ) {
									// TODO a way to configure the attribute name. Or get rid of name?
									m.plus( "@type" to TextNode(name) )
								} else {
									m
								}
							}
							.toMap()
			)
			is FigList -> ArrayNode(
					JsonNodeFactory.instance,
					list.map { it.toJsonNode() }
			)
			is FigNumber -> DecimalNode(value)
			is FigString -> TextNode(value)
			is FigBoolean -> if ( value ) BooleanNode.TRUE else BooleanNode.FALSE
		}
